import { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";

function App() {
  const [count, setCount] = useState(0);
  const [rgb, setRGB] = useState([0, 0, 0]);

  const changeColorHandler = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();

    const [r, g, b] = rgb.map((x) => Math.floor(Math.random() * 256));
    setRGB([r, g, b]);

    return false;
  };

  useEffect(() => {
    const [r, g, b] = rgb;
    const headerElement = document.querySelector("header");
    if (headerElement) {
      headerElement.style.backgroundColor = `rgb(${r}, ${g}, ${b})`;
    }
  }, [rgb]);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Hello Vite + React!</p>
        <p>{rgb.join(", ")}</p>
        <p>
          <button
            type="button"
            onClick={(e) => {
              setCount((count) => count + 1);
              changeColorHandler(e);
            }}
          >
            count is: {count}
          </button>
        </p>
        <p>
          Edit <code>App.tsx</code> and save to test HMR updates.
        </p>
        <p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          {" | "}
          <a
            className="App-link"
            href="https://vitejs.dev/guide/features.html"
            target="_blank"
            rel="noopener noreferrer"
          >
            Vite Docs
          </a>
        </p>
      </header>
    </div>
  );
}

export default App;
